import os
import sys

import dns.name
import pyperf

sys.path.append(os.path.dirname(os.getcwd()))

import dike.label  # noqa: E402
import dike.name   # noqa: E402


def bench_name(loops, vals):
    range_it = range(loops)
    t0 = pyperf.perf_counter()

    for _ in range_it:
        dike.name.Name(vals)

    return pyperf.perf_counter() - t0


def bench_dnspython_name(loops, vals):
    range_it = range(loops)
    t0 = pyperf.perf_counter()

    for _ in range_it:
        dns.name.Name(vals)

    return pyperf.perf_counter() - t0


def bench_name_fromstr(loops, val):
    range_it = range(loops)
    t0 = pyperf.perf_counter()

    for _ in range_it:
        dike.name.Name.fromstr(val)

    return pyperf.perf_counter() - t0


def bench_dnspython_name_from_text(loops, val):
    range_it = range(loops)
    t0 = pyperf.perf_counter()

    for _ in range_it:
        dns.name.from_text(val)

    return pyperf.perf_counter() - t0


def bench_name_gt(loops, n1, n2):
    range_it = range(loops)
    t0 = pyperf.perf_counter()

    for _ in range_it:
        n1 > n2

    return pyperf.perf_counter() - t0


def bench_name_eq(loops, n1, n2):
    range_it = range(loops)
    t0 = pyperf.perf_counter()

    for _ in range_it:
        n1 == n2

    return pyperf.perf_counter() - t0


def main():
    runner = pyperf.Runner()

    vals = (
        b'www',
        b'measuring',
        b'com',
        b'test',
    )
    n1 = dike.name.Name.fromstr("www.example.com")
    n2 = dike.name.Name.fromstr("www.xample.com")
    p1 = dns.name.from_text("www.example.com")
    p2 = dns.name.from_text("www.xample.com")
    tests = [
        ["new name (root) - dike",
         bench_name, (tuple(),)],
        ["new name (root) - dnspython",
         bench_dnspython_name, (tuple(),)],
        ["new name (TLD) - dike",
         bench_name, (vals[-1:],)],
        ["new name (TLD) - dnspython",
         bench_dnspython_name, (vals[-1:],)],
        ["new name (2LD) - dike",
         bench_name, (vals[-2:],)],
        ["new name (2LD) - dnspython",
         bench_dnspython_name, (vals[-2:],)],
        ["new name (3-labels) - dike",
         bench_name, (vals[-3:],)],
        ["new name (3-labels) - dnspython",
         bench_dnspython_name, (vals[-3:],)],
        ["new name (4-labels) - dike",
         bench_name, (vals[-4:],)],
        ["new name (4-labels) - dnspython",
         bench_dnspython_name, (vals[-4:],)],
        ["new name - Name.fromstr() - dike",
         bench_name_fromstr, ("mx.tld.test",)],
        ["new name - name.from_text - dnspython",
         bench_dnspython_name_from_text, ("mx.tld.test",)],
        ["name comparison, greater than (>) - dike",
         bench_name_gt, (n1, n2,)],
        ["name comparison, greater than (>) - dnspython",
         bench_name_gt, (p1, p2,)],
        ["name comparison, equality (==) - dike",
         bench_name_eq, (n1, n2,)],
        ["name comparison, equality (==) - dnspython",
         bench_name_eq, (p1, p2,)],
    ]
    for test_name, test_func, test_args in tests:
        runner.bench_time_func(test_name, test_func, *test_args)


if __name__ == '__main__':
    main()
