# Overview

In this directory you can find some micro-benchmarks of the library.
We do some comparisons with dnspython, since ideally we would like to
be at least that fast.

We use the excellent pyperf module for measurement.

# Setup

You need to install the requirements, something like this:

```
python3 -m pip install -r bench-requirements.txt
```

# Running

You can run each benchmark independently, just by running the file
like so:

```
python3 bench-name.py
```

You can run all benchmarks using the `Makefile`:

```
make
```
