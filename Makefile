all: FORCE
	flake8 \
        dike/errors.py \
        dike/name.py dike/test_name.py \
		dike/constants.py dike/test_constants.py \
        dike/edns.py \
        dike/message.py \
		dike/question.py \
        dike/rr.py \
        dike/section.py \
        dike/util.py \
		dike/rdata_a.py \
        dike/rdata_cname.py \
        dike/rdata_ns.py \
		dike/rdata_ptr.py \
        dike/rdata_soa.py \
        dike/rdata_mx.py \
		dike/rdata_txt.py \
        dike/rdata_aaaa.py \
        dike/rdata_rrsig.py \
        benchmark/bench-name.py
	
	pylint dike/errors.py dike/name.py dike/constants.py
	
	mypy --strict --follow-imports=silent \
        dike/errors.py \
        dike/name.py dike/test_name.py \
        dike/constants.py dike/test_constants.py

test: FORCE
	pytest -v dike/test_name.py dike/test_constants.py

coverage: FORCE
	pytest --cov=dike.name --cov=dike.constants --cov=dike.errors \
	--cov-report=term-missing --cov-branch \
	dike/test_name.py dike/test_constants.py

doc: FORCE
	cd doc && make html

FORCE:
    
