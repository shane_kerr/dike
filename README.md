**WARNING** - the Dike library is not ready for use.

# Introduction
The Dike DNS Library is intended to be a powerful and Pythonic library
for working with the DNS. It is a pure Python tool that aims to be
both correct and efficient, both for programmer time and for runtime
performance.

# Current Status
The library currently has some classes for DNS names, as well as basic
packet parsing and assembling.

# Usage

You can do something like this:

```python
import dike

myname = dike.make_name("foo.bar")
if myname.endswith("bar"):
    print(f"{myname} ends with 'bar'")
```

# Documentation

The latest documentation is published on the readthedocs.io site:

https://dike.readthedocs.io/en/latest/

Check out the `doc/` directory, where the Sphinx documentation for the
libary lives.

# Working with the code

You need to install the required libraries and then you can run the
various tests.

```bash
$ python3 -m pip install -r dev-requirements.txt
$ make
$ make test
$ make coverage
```

While the code runs under pypy3, development requires CPython, since
the `mypy` static checker only works under the latest CPython.
