import socket

import dike


def main():
    hdr = dike.Header(rd=1)
    question = dike.Question(dike.Name.fromstr("www.ns.nl"))
    edns = dike.EDNS()
    query = dike.Message(hdr, question, edns=edns)

    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.connect(('9.9.9.9', 53))
    sock.send(query.to_wire())

    # XXX: we should have timeouts
    # XXX: we should loop until we get the correct message ID and question
    pkt = sock.recv(65536)
    reply = dike.MessagePacket(pkt)
    print("\n".join(reply.dig_format()))

if __name__ == '__main__':
    main()
