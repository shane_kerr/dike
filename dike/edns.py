import struct

from dike.constants import RCODE


class OptRR:
    def __init__(self, code, data):
        self.code = code
        self.data = data

    def to_wire(self):
        return struct.pack("!HH", self.code, len(self.data)) + self.data


class EDNS:
    def __init__(self, *,
                 payload_size=1220, extended_rcode=RCODE.NOERROR,
                 version=0, do=1,
                 opt_rrs=None):
        self.payload_size = payload_size
        self.extended_rcode = extended_rcode
        self.version = version
        self.do = do
        if opt_rrs is None:
            self.opt_rrs = []
        else:
            self.opt_rrs = opt_rrs
        # by type
        # self.opt_rrs_by_type = {}

    def to_wire(self):
        flags = self.do << 15
        opt_rrdata = b''.join(opt_rr.to_wire() for opt_rr in self.opt_rrs)
        return (b'\0\0\x29' +
                struct.pack("!HBBHH",
                            self.payload_size,
                            self.extended_rcode,
                            self.version,
                            flags,
                            len(opt_rrdata),
                            ) +
                opt_rrdata)


class EDNS_RR_Iterator:
    def __init__(self, edns):
        self.edns = edns
        self.rr_idx = 0

    def next(self):
        if self.rr_idx >= len(self.opt_rrs):
            if self.edns.rrdata_ofs >= len(self.edns.rrdata):
                raise StopIteration()
            self.edns._parse_next_opt_rr()
        opt_rr = self.opt_rrs[self.rr_idx]
        self.rr_idx += 1
        return opt_rr


class EDNSfromRR(EDNS):
    def __init__(self, *,
                 payload_size, extended_rcode, version, do, rrdata):
        self.payload_size = payload_size
        self.extended_rcode = extended_rcode
        self.version = version
        self.do = do
        self.rrdata = rrdata
        self.opt_rrs = []
        self.rrdata_ofs = 0

    def __iter__(self):
        return EDNS_RR_Iterator(self)

    def _parse_next_opt_rr(self):
        ofs = self.rrdata_ofs
        (opt_code,
         opt_rrdata_len) = struct.unpack("!HH", self.rrdata[ofs:ofs+4])
        opt_rrdata = self.rrdata[ofs+4:ofs+4+opt_rrdata_len]
        self.rrdata_ofs += 4+opt_rrdata_len
        self.opt_rrs.append(OptRR(opt_code, opt_rrdata))
