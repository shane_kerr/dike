import numbers
import struct

import dike.constants
from dike.name import Name
import dike.rr
from dike.util import _get_name_from_packet

# TODO: memoize conversions?


class rdata_soa(dike.rr.RData):
    """
    mname - dike.Name() instance
    rname - dike.Name() instance
    serial - integer between 0 and 4294967295
    refresh - integer between 0 and 4294967295
    retry - integer between 0 and 4294967295
    expire - integer between 0 and 4294967295
    minimum - integer between 0 and 4294967295
    """
    def __init__(self, *,
                 mname, rname, serial, refresh, retry, expire, minimum):
        self.mname = mname
        self.rname = rname
        self.serial = serial
        self.refresh = refresh
        self.retry = retry
        self.expire = expire
        self.minimum = minimum

    def _invariant(self):
        self.mname._invariant()
        self.rname._invariant()
        assert isinstance(self.serial, numbers.Integral)
        assert 0 <= self.serial < 2**32
        assert isinstance(self.refresh, numbers.Integral)
        assert 0 <= self.refresh < 2**32
        assert isinstance(self.retry, numbers.Integral)
        assert 0 <= self.retry < 2**32
        assert isinstance(self.expire, numbers.Integral)
        assert 0 <= self.expire < 2**32
        assert isinstance(self.minimum, numbers.Integral)
        assert 0 <= self.minimum < 2**32

    # length without name compression
    def __len__(self):
        return len(self.mname.to_wire()) + len(self.rname.to_wire()) + 20

    # def to_wire(self):
    #    fixed_fields = struct.pack("!IIIII", self.serial, self.refresh,
    #                               self.retry, self.expire, self.minimum)
    #    return (self.mname.to_wire() + self.rname.to_wire() + fixed_fields)

    def to_presentation(self):
        return (self.mname.to_presentation() + " " +
                self.rname.to_presentation() + " " +
                str(self.serial) + " " +
                str(self.refresh) + " " +
                str(self.retry) + " " +
                str(self.expire) + " " +
                str(self.minimum))


class SOATooShort(dike.rr.RRTooShort):
    pass


class rdata_packet_soa(rdata_soa):
    def __init__(self, pkt, ofs, rr_len, name_cache=None):
        self.pkt = pkt
        self.ofs = ofs
        self.soa_len = rr_len
        if name_cache is None:
            self.name_cache = {}
        else:
            self.name_cache = name_cache

        self._mname_labels = None
        self._rname_ofs = None
        self._rname_labels = None
        self._fixed_ofs = None

    def __dir__(self):
        return ["mname", "rname",
                "serial",
                "refresh", "retry", "expire",
                "minimum",
                "__init__", "__len__", "__dir__", "to_presentation",
                "_invariant"]

    def _get_mname_labels(self):
        try:
            (self._mname_labels,
             mname_len) = _get_name_from_packet(self.pkt, self.ofs,
                                                self.name_cache)
        except IndexError:
            raise SOATooShort()
        self._rname_ofs = self.ofs + mname_len

    def _get_rname_labels(self):
        if self._mname_labels is None:
            self._get_mname_labels()

        try:
            (self._rname_labels,
             rname_len) = _get_name_from_packet(self.pkt, self._rname_ofs,
                                                self.name_cache)
        except IndexError:
            raise SOATooShort()
        self._fixed_ofs = self._rname_ofs + rname_len

    def __getattr__(self, name):
        if name == "mname":
            if self._mname_labels is None:
                self._get_mname_labels()
            self.mname = Name(self._mname_labels)
            return self.mname

        if name == "rname":
            if self._rname_labels is None:
                self._get_rname_labels()
            self.rname = Name(self._rname_labels)
            return self.rname

        if name not in ("serial", "refresh", "retry", "expire", "minimum"):
            msg = f"'{type(self).__name__}' object has no attribute '{name}'"
            raise AttributeError(msg)

        if self._fixed_ofs is None:
            self._get_rname_labels()

        try:
            buf = self.pkt[self._fixed_ofs:self._fixed_ofs+20]
            (self.serial,
             self.refresh,
             self.retry,
             self.expire,
             self.minimum) = struct.unpack("!IIIII", buf)
        except (struct.error, IndexError):
            raise SOATooShort()

        return getattr(self, name)

    def __len__(self):
        return self.soa_len


dike.rr.register_rr_packet_type(dike.constants.Type.SOA, rdata_packet_soa)
