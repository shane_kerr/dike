import secrets
import struct

from dike.constants import OpCode, RCODE


class Header:
    def __init__(self, *,
                 id=None,
                 qr=0, opcode=0, aa=0, tc=0, rd=0, ra=0, ad=0, cd=0,
                 rcode=0,
                 qdcount=0,
                 ancount=0,
                 nscount=0,
                 arcount=0):
        if id is None:
            id = secrets.randbits(16)
        self.id = id
        self.qr = qr
        self.opcode = opcode
        self.aa = aa
        self.tc = tc
        self.rd = rd
        self.ra = ra
        self.ad = ad
        self.cd = cd
        self.rcode = rcode
        self.qdcount = qdcount
        self.ancount = ancount
        self.nscount = nscount
        self.arcount = arcount

    def to_wire(self):
        flags = ((self.qr << 15) |
                 (self.opcode << 11) |
                 (self.aa << 10) |
                 (self.tc << 9) |
                 (self.rd << 8) |
                 (self.ra << 7) |
                 (self.ad << 5) |
                 (self.cd << 4) |
                 (self.rcode))
        return struct.pack("!HHHHHH",
                           self.id,
                           flags,
                           self.qdcount,
                           self.ancount,
                           self.nscount,
                           self.arcount)

    def dig_format(self):
        # NOTE: This format does not show if the reserved flag bit is set.
        #       BIND adds "; MBZ: 0x4 " after the flags if it is present.

        try:
            opcode = OpCode(self.opcode).name
        except ValueError:
            opcode = f"RESERVED{self.opcode}"

        try:
            rcode = RCODE(self.rcode).name.upper()
        except ValueError:
            rcode = f"RESERVED{self.rcode}"

        flags = []
        for flag in ("qr", "aa", "tc", "rd", "ra", "ad", "cd"):
            if getattr(self, flag):
                flags.append(flag)
        flags_str = " ".join(flags)

        counts = []
        label_count = (("QUERY", "qdcount"), ("ANSWER", "ancount"),
                       ("AUTHORITY", "nscount"), ("ADDITIONAL", "arcount"))
        for label, count in label_count:
            counts.append(f"{label}: {getattr(self, count)}")
        counts_str = ", ".join(counts)
        fmt0 = (f";; ->>HEADER<<- opcode: {opcode}, "
                f"status: {rcode}, id: {self.id}")
        fmt1 = f";; flags: {flags_str}; {counts_str}"
        return (fmt0, fmt1)

#    def tcpdump_format(self):
#        return ""


class HeaderPacketTooShort(Exception):
    pass


class HeaderPacket(Header):
    def __init__(self, pkt):
        self._pkt = pkt
        self._parsed = False

    def parse(self):
        if self._parsed:
            return

        if len(self._pkt) < 12:
            msg = f"packet must be at least 12 bytes, size is {len(self._pkt)}"
            raise HeaderPacketTooShort(msg)

        (self.id,
         self._flags,
         self.qdcount,
         self.ancount,
         self.nscount,
         self.arcount,) = struct.unpack("!HHHHHH", self._pkt[0:12])

        self._parsed = True

    def _get_qr(self):
        self.qr = self._flags >> 15
        return self.qr

    def _get_opcode(self):
        self.opcode = (self._flags >> 11) & 0xF
        return self.opcode

    def _get_aa(self):
        self.aa = (self._flags >> 10) & 1
        return self.aa

    def _get_tc(self):
        self.tc = (self._flags >> 9) & 1
        return self.tc

    def _get_rd(self):
        self.rd = (self._flags >> 8) & 1
        return self.rd

    def _get_ra(self):
        self.ra = (self._flags >> 7) & 1
        return self.ra

    def _get_ad(self):
        self.ad = (self._flags >> 5) & 1
        return self.ad

    def _get_cd(self):
        self.cd = (self._flags >> 4) & 1
        return self.cd

    def _get_rcode(self):
        self.rcode = self._flags & 0xF
        return self.rcode

    _fixed_attrs = set([
        "id",
        "qdcount",
        "ancount",
        "nscount",
        "arcount",
    ])

    _bit_attrs = {
        "qr": _get_qr,
        "opcode": _get_opcode,
        "aa": _get_aa,
        "tc": _get_tc,
        "rd": _get_rd,
        "ra": _get_ra,
        "ad": _get_ad,
        "cd": _get_cd,
        "rcode": _get_rcode,
    }

    def __getattr__(self, name):
        self.parse()

        if name in self._bit_attrs:
            return self._bit_attrs[name](self)

        if name not in self._fixed_attrs:
            msg = f"'{type(self).__name__}' object has no attribute '{name}'"
            ex = AttributeError(msg)
            ex.__suppress_context__ = True
            raise ex

        return getattr(self, name)

    # TODO: Think of a faster way to build responses.
    #       Like... look for fields that have changed? We could hook
    #       into the __setattr__() method for this purpose.

    def size(self):
        return 12
