# XXX: keep label cache optional?
import struct

from dike.name import Name, make_name
from dike.constants import Class, class_name, Type, type_name
from dike.util import _get_name_from_packet


class Question:
    def __init__(self, qname, qtype=Type.AAAA, qclass=Class.IN):
        self.qname = qname
        self.qtype = qtype
        self.qclass = qclass

    def dig_format(self):
        return (f";{self.qname.to_presentation()}\t\t"
                f"{class_name(self.qclass)}\t"
                f"{type_name(self.qtype)}")

    def to_wire(self):
        return (self.qname.to_wire() +
                struct.pack("!HH", self.qtype, self.qclass))

    def __eq__(self, other):
        return ((self.qtype == other.qtype) and
                (self.qclass == other.qclass) and
                (self.qname == other.qname))

    def __hash__(self):
        labels = [self.qname[i].canonical() for i in range(len(self.qname))]
        return hash(tuple(labels) + (self.qtype, self.qclass))


def make_question(qname, qtype=Type.AAAA, qclass=Class.IN):
    return Question(make_name(qname), qtype, qclass)


class QuestionTooShort(Exception):
    pass


class QuestionPacket(Question):
    def __init__(self, pkt, ofs=12, name_cache=None):
        self._pkt = pkt
        self._ofs = ofs
        if name_cache is None:
            self._name_cache = {}
        else:
            self._name_cache = name_cache
        self._parsed = False

    def _parse(self):
        if self._parsed:
            return

        try:
            (self._qname_labels,
             qname_len) = _get_name_from_packet(self._pkt,
                                                self._ofs,
                                                self._name_cache)
        except IndexError:
            raise QuestionTooShort()

        self._question_size = qname_len + 4
        if (self._ofs + self._question_size) > len(self._pkt):
            raise QuestionTooShort()

        self._parsed = True

    def parse(self):
        self._parse()
        # The _parse() method gets the fixed-length fields and splits
        # up the names into labels, but does not check that the total
        # wire name length is less <= 255 bytes. We'll go ahead and
        # instantiate the Name object, which *does* perform that
        # check.
        if getattr(self, 'qname', None) is None:
            self.qname = Name(self._qname_labels)

    def __getattr__(self, name):
        self._parse()

        if name in ("qtype", "qclass"):
            ofs = self._ofs + self._question_size
            (self.qtype,
             self.qclass) = struct.unpack("!HH", self._pkt[ofs-4:ofs])
            return getattr(self, name)
        elif name == "qname":
            self.qname = Name(self._qname_labels)
            return self.qname
        else:
            msg = f"'{type(self).__name__}' object has no attribute '{name}'"
            raise AttributeError(msg)

    # Use a special to_wire() method since we don't have to instantiate
    # a Name object for the qname.
    def to_wire(self):
        self._parse()
        wire_labels = [(chr(len(label)).encode() + label)
                       for label in self._qname_labels]
        return (b''.join(wire_labels) + b'\0' +
                struct.pack("!HH", self.qtype, self.qclass))

    def size(self):
        self._parse()
        return self._question_size
