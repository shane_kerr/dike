import dike.constants
import dike.rr
from dike.util import escape_char_str


class rdata_txt(dike.rr.RData):
    """
    txt_data - iterable of bytes
    """
    def __init__(self, *, txt_data):
        self.txt_data = txt_data

    def _invariant(self):
        for char_str in self.txt_data:
            assert isinstance(char_str, bytes)
            assert len(char_str) <= 255

    # memoize?
    def __len__(self):
        return len(self.txt_data) + sum([len(cs) for cs in self.txt_data])

    # memoize?
    def to_presentation(self):
        return " ".join([escape_char_str(cs) for cs in self.txt_data])


class TXTTooShort(dike.rr.RRTooShort):
    pass


class rdata_packet_txt(rdata_txt):
    def __init__(self, pkt, ofs, rr_len, name_cache=None):
        self.pkt = pkt
        self.ofs = ofs
        self.txt_len = rr_len
        if name_cache is None:
            self.name_cache = {}
        else:
            self.name_cache = name_cache

    def __dir__(self):
        return ["txt_data",
                "__init__", "__len__", "__dir__", "to_presentation",
                "_invariant"]

    def __getattr__(self, name):
        if name == "txt_data":
            txt_data = []
            try:
                pos = self.ofs
                end_pos = self.ofs+self.txt_len
                while pos < end_pos:
                    char_str_len = self.pkt[pos]
                    pos += 1
                    char_str = self.pkt[pos:pos+char_str_len]
                    txt_data.append(char_str)
                    pos += char_str_len
            except IndexError:
                return TXTTooShort()
            self.txt_data = txt_data
            return txt_data

        msg = f"'{type(self).__name__}' object has no attribute '{name}'"
        raise AttributeError(msg)

    def __len__(self):
        return self.txt_len


dike.rr.register_rr_packet_type(dike.constants.Type.TXT, rdata_packet_txt)
