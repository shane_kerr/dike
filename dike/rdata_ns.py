import dike.constants
from dike.name import Name
import dike.rr
from dike.util import _get_name_from_packet

# TODO: memoize conversions?


class rdata_ns(dike.rr.RData):
    """
    nsdname - dike.Name() instance
    """
    def __init__(self, *, nsdname):
        self.nsdname = nsdname

    def _invariant(self):
        self.nsdname._invariant()

    # length without name compression
    def __len__(self):
        return len(self.nsdname.to_wire())

    def to_presentation(self):
        return self.nsdname.to_presentation()


class NSTooShort(dike.rr.RRTooShort):
    pass


class rdata_packet_ns(rdata_ns):
    def __init__(self, pkt, ofs, rr_len, name_cache=None):
        self.pkt = pkt
        self.ofs = ofs
        self.ns_len = rr_len
        if name_cache is None:
            self.name_cache = {}
        else:
            self.name_cache = name_cache

    def __dir__(self):
        return ["nsdname",
                "__init__", "__len__", "__dir__", "to_presentation",
                "_invariant"]

    def __getattr__(self, name):
        if name == "nsdname":
            try:
                (nsdname_labels,
                 nsdname_len) = _get_name_from_packet(self.pkt, self.ofs,
                                                      self.name_cache)
            except IndexError:
                raise NSTooShort()
            self.nsdname = Name(nsdname_labels)
            return self.nsdname

        msg = f"'{type(self).__name__}' object has no attribute '{name}'"
        raise AttributeError(msg)

    def __len__(self):
        return self.ns_len


dike.rr.register_rr_packet_type(dike.constants.Type.NS, rdata_packet_ns)
