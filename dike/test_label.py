import unittest

from dike.errors import EmptyLabel, LabelTooLong
import dike.label as label


class TestLabel(unittest.TestCase):
    def test_init_from_byte(self) -> None:
        # check a normal case
        the_label = label.Label(b'normal')
        the_label._invariant()
        self.assertEqual(bytes(the_label), b'normal')
        # check the empty case
        with self.assertRaises(EmptyLabel):
            label.Label(b'')
        # check edge cases
        the_label = label.Label(b'x')
        the_label._invariant()
        self.assertEqual(bytes(the_label), b'x')
        the_label = label.Label(b'y' * 63)
        the_label._invariant()
        self.assertEqual(bytes(the_label), b'y' * 63)
        # check a name too long
        with self.assertRaises(LabelTooLong):
            label.Label(b'z' * 64)
        # check a name *really* too long
        with self.assertRaises(LabelTooLong):
            label.Label(b'z' * 6400)

    def test_init_canonicalize(self) -> None:
        label1 = label.Label(b'normal', canonicalize=True)
        label1._invariant()
        self.assertEqual(bytes(label1), b'normal')
        label2 = label.make_label(label1, canonicalize=True)
        label2._invariant()
        self.assertEqual(bytes(label2), b'normal')
        label1 = label.Label.fromstr('Normal', canonicalize=True)
        label1._invariant()
        self.assertEqual(bytes(label1), b'normal')
        label2 = label.make_label(label1, canonicalize=True)
        label2._invariant()
        self.assertEqual(bytes(label2), b'normal')

    def test_init_from_str(self) -> None:
        # check a normal case
        the_label = label.Label.fromstr('normal')
        the_label._invariant()
        self.assertEqual(bytes(the_label), b'normal')
        # check the empty case
        with self.assertRaises(EmptyLabel):
            label.Label.fromstr('')
        # check edge cases
        the_label = label.Label.fromstr('x')
        the_label._invariant()
        self.assertEqual(bytes(the_label), b'x')
        the_label = label.Label.fromstr('y' * 63)
        the_label._invariant()
        self.assertEqual(bytes(the_label), b'y' * 63)
        # check a name too long
        with self.assertRaises(LabelTooLong):
            label.Label.fromstr('z' * 64)
        # check a name *really* too long
        with self.assertRaises(LabelTooLong):
            label.Label.fromstr('z' * 6400)

    def test_init_from_unicode(self) -> None:
        # check non-ASCII characters
        the_label = label.Label.fromstr('☺')
        the_label._invariant()
        self.assertEqual(bytes(the_label), b'xn--74h')
        # check mix of ASCII and non-ASCII
        the_label = label.Label.fromstr('happy-☺')
        the_label._invariant()
        self.assertEqual(bytes(the_label), b'xn--happy--1g9c')
        # check case of normal name
        the_label = label.Label.fromstr('IamSam')
        the_label._invariant()
        self.assertEqual(bytes(the_label), b'IamSam')
        # check case of mixed ASCII with non-ASCII
        the_label = label.Label.fromstr('SamIam😝')
        the_label._invariant()
        self.assertEqual(bytes(the_label), b'xn--samiam-1174e')
        # check strings that cannot be converted to IDNA labels
        with self.assertRaises(UnicodeError):
            label.Label.fromstr(chr(0x80))

    def test_canonical(self) -> None:
        # test normal case
        the_label = label.Label(b'normal')
        the_label._invariant()
        self.assertEqual(the_label.canonical(), b'normal')
        # test all-uppercase
        the_label = label.Label(b'SHOUTING')
        the_label._invariant()
        self.assertEqual(the_label.canonical(), b'shouting')
        # test mixed-case
        the_label = label.Label(b'BIG-little-BIG')
        the_label._invariant()
        self.assertEqual(the_label.canonical(), b'big-little-big')

    def test_ishost(self) -> None:
        # check a normal case
        the_label = label.Label(b'normal')
        the_label._invariant()
        self.assertTrue(the_label.ishost())
        self.assertTrue(the_label.ishost())
        the_label._invariant()
        # check edge cases
        the_label = label.Label(b'x')
        the_label._invariant()
        self.assertTrue(the_label.ishost())
        self.assertTrue(the_label.ishost())
        the_label = label.Label(b'y' * 63)
        the_label._invariant()
        self.assertTrue(the_label.ishost())
        self.assertTrue(the_label.ishost())
        # check non-alphanumeric start
        the_label = label.Label(b'+')
        the_label._invariant()
        self.assertFalse(the_label.ishost())
        self.assertFalse(the_label.ishost())
        the_label = label.Label(b'-')
        the_label._invariant()
        self.assertFalse(the_label.ishost())
        self.assertFalse(the_label.ishost())
        # check non-alphanumeric end
        the_label = label.Label(b's+')
        the_label._invariant()
        self.assertFalse(the_label.ishost())
        self.assertFalse(the_label.ishost())
        the_label = label.Label(b'r-')
        the_label._invariant()
        self.assertFalse(the_label.ishost())
        self.assertFalse(the_label.ishost())
        # check middle hyphen
        the_label = label.Label(b'a-b')
        the_label._invariant()
        self.assertTrue(the_label.ishost())
        self.assertTrue(the_label.ishost())
        the_label = label.Label(b'a-b-c')
        the_label._invariant()
        self.assertTrue(the_label.ishost())
        self.assertTrue(the_label.ishost())
        the_label = label.Label(b'a--d')
        the_label._invariant()
        self.assertTrue(the_label.ishost())
        self.assertTrue(the_label.ishost())
        the_label = label.Label(b'e---f')
        the_label._invariant()
        self.assertTrue(the_label.ishost())
        self.assertTrue(the_label.ishost())
        # check middle non-hyphen
        the_label = label.Label(b'g-^-h')
        the_label._invariant()
        self.assertFalse(the_label.ishost())
        self.assertFalse(the_label.ishost())

    def test_cmp(self) -> None:
        # labels for various comparisons
        label_a = label.Label(b'a')
        label_a._invariant()
        label_b = label.Label(b'b')
        label_b._invariant()
        label_big_a = label.Label(b'A')
        label_big_a._invariant()
        label_ab = label.Label(b'ab')
        label_ab._invariant()
        # a vs. a
        self.assertGreaterEqual(label_a, label_a)
        self.assertLessEqual(label_a, label_a)
        self.assertEqual(label_a, label_a)
        # a vs. b
        self.assertNotEqual(label_a, label_b)
        self.assertLess(label_a, label_b)
        self.assertLessEqual(label_a, label_b)
        self.assertGreater(label_b, label_a)
        self.assertGreaterEqual(label_b, label_a)
        # a vs. A
        self.assertEqual(label_a, label_big_a)
        self.assertGreaterEqual(label_a, label_big_a)
        self.assertLessEqual(label_a, label_big_a)
        self.assertGreaterEqual(label_big_a, label_a)
        self.assertLessEqual(label_big_a, label_a)
        # a vs. ab
        self.assertNotEqual(label_a, label_ab)
        self.assertLess(label_a, label_ab)
        self.assertLessEqual(label_a, label_ab)
        self.assertGreater(label_ab, label_a)
        self.assertGreaterEqual(label_ab, label_a)
        # test comparison with strings
        self.assertEqual(label_a, "a")
        self.assertNotEqual(label_a, "b")
        with self.assertRaises(LabelTooLong):
            self.assertNotEqual(label_a, "b" * 350)
        self.assertEqual(label_a, "A")
        # test comparison with bytes
        self.assertEqual(label_a, b"a")
        self.assertNotEqual(label_a, b"b")
        self.assertEqual(label_a, b"A")
        with self.assertRaises(LabelTooLong):
            self.assertNotEqual(label_a, b"b" * 350)
        # test comparison of empty labels
        self.assertNotEqual(label_a, '')
        self.assertNotEqual(label_a, b'')
        # test equality with unsuported types
        self.assertNotEqual(label_a, 1234)
        self.assertFalse(label_a == 1234)
        # test comparison with bad Unicode
        with self.assertRaises(UnicodeError):
            label_a == chr(0x80)

    def test_hash(self) -> None:
        label_a = label.Label(b'a')
        label_a._invariant()
        label_b = label.Label.fromstr('b')
        label_b._invariant()
        label_big_a = label.Label.fromstr('A')
        label_big_a._invariant()
        self.assertEqual(hash(label_a), hash(label_a))
        self.assertNotEqual(hash(label_a), hash(label_b))
        self.assertEqual(hash(label_a), hash(label_big_a))
        self.assertEqual(hash(label_b), hash(label_b))
        self.assertNotEqual(hash(label_b), hash(label_big_a))
        self.assertEqual(hash(label_big_a), hash(label_big_a))

    def test_repr(self) -> None:
        label_a = label.Label(b'a')
        label_a._invariant()
        self.assertEqual(repr(label_a), "Label.fromstr('a')")
        label_b = label.Label(b'\x80')
        label_b._invariant()
        self.assertEqual(repr(label_b), "Label(b'\\x80')")

    def test_str(self) -> None:
        label_a = label.Label(b'a')
        label_a._invariant()
        self.assertEqual(str(label_a), 'a')
        label_a = label.Label.fromstr('😝')
        label_a._invariant()
        self.assertEqual(str(label_a), '😝')
        notUnicode = label.Label(b'\x80')
        with self.assertRaises(UnicodeDecodeError):
            str(notUnicode)

    def test_bytes(self) -> None:
        label_a = label.Label(b'a')
        label_a._invariant()
        self.assertEqual(bytes(label_a), b'a')
        label_a = label.Label.fromstr('😝')
        label_a._invariant()
        self.assertEqual(bytes(label_a), b'xn--728h')

    def test_to_presentation(self) -> None:
        for ch in range(0, 256):
            the_label = label.Label(bytes([ch]))
            if ch == ord(' '):
                self.assertEqual("\\032", the_label.to_presentation())
            elif ch == ord('"'):
                self.assertEqual('\\"', the_label.to_presentation())
            elif ch == ord('$'):
                self.assertEqual('\\$', the_label.to_presentation())
            elif ch == ord('('):
                self.assertEqual('\\(', the_label.to_presentation())
            elif ch == ord(')'):
                self.assertEqual('\\)', the_label.to_presentation())
            elif ch == ord('.'):
                self.assertEqual('\\.', the_label.to_presentation())
            elif ch == ord(';'):
                self.assertEqual('\\;', the_label.to_presentation())
            elif ch == ord('@'):
                self.assertEqual('\\@', the_label.to_presentation())
            elif ch == ord('\\'):
                self.assertEqual('\\092', the_label.to_presentation())
            elif (0 < ch < 128) and chr(ch).isprintable():
                self.assertEqual(chr(ch), the_label.to_presentation())
            else:
                self.assertEqual("\\%03d" % ch, the_label.to_presentation())

    def test_make_label(self) -> None:
        a_label = label.make_label(b'123')
        a_label._invariant()
        self.assertEqual(a_label, b'123')
        b_label = label.make_label('Stringy')
        b_label._invariant()
        self.assertEqual(b_label, b'Stringy')
        c_label = label.make_label(a_label)
        c_label._invariant()
        self.assertEqual(c_label, a_label)
        d_label = label.make_label(b_label, canonicalize=True)
        d_label._invariant()
        self.assertEqual(d_label, b_label)
        self.assertNotEqual(bytes(d_label), bytes(b_label))


class TestLabelFactory(unittest.TestCase):
    def test_init(self) -> None:
        the_factory = label.LabelFactory()
        the_factory._invariant()

    def test_frombytes(self) -> None:
        the_factory = label.LabelFactory()
        # verify happy path
        label_a = the_factory.frombytes(b'xxx')
        label_b = the_factory.frombytes(b'xxx')
        self.assertIs(label_a, label_b)
        # verify that different inputs give different labels
        label_c = the_factory.frombytes(b'xyz')
        self.assertFalse(label_a is label_c)
        # verify that labels created outside of a factory are unique
        label_d = label.Label(b'xxx')
        self.assertFalse(label_a is label_d)
        # verify behavior is case-insensitive
        label_c = the_factory.frombytes(b'xXx')
        self.assertIs(label_a, label_c)
        self.assertIs(label_b, label_c)
        # verify empty bytes raise an exception
        with self.assertRaises(EmptyLabel):
            the_factory.frombytes(b'')
        # verify behavior if we pass too many bytes
        with self.assertRaises(LabelTooLong):
            the_factory.frombytes(b'x' * 64)

    def test_fromlabel(self) -> None:
        the_factory = label.LabelFactory()

        # verify happy path
        label_a = label.Label(b'xxx')
        label_b = the_factory.fromlabel(label_a)
        self.assertIs(label_a, label_b)

        # verify behavior if we have added a label already

        # In this case the factory will return the previously added
        # label instance, and not the one that we are using to
        # initialize.
        label_c = label.Label(b'xxx')
        label_d = the_factory.fromlabel(label_c)
        self.assertFalse(label_c is label_d)
        self.assertIs(label_a, label_d)

        # verify that different inputs give different labels
        label_e = label.Label(b'xyz')
        label_f = the_factory.fromlabel(label_e)
        self.assertFalse(label_a is label_f)

        # verify that labels created outside of a factory are unique
        label_g = label.Label(b'xxx')
        self.assertFalse(label_a is label_g)

        # verify behavior is case-insensitive
        label_h = label.Label(b'xXx')
        label_i = the_factory.fromlabel(label_h)
        self.assertIs(label_a, label_i)

    def test_fromstr(self) -> None:
        the_factory = label.LabelFactory()
        # verify happy path
        label_a = the_factory.fromstr('xxx')
        label_b = the_factory.fromstr('xxx')
        self.assertIs(label_a, label_b)
        # verify that different inputs give different labels
        label_c = the_factory.fromstr('xyz')
        self.assertFalse(label_a is label_c)
        # verify that labels created outside of a factory are unique
        label_d = label.Label.fromstr('xxx')
        self.assertFalse(label_a is label_d)
        # verify behavior is case-insensitive
        label_c = the_factory.fromstr('xXx')
        self.assertIs(label_a, label_c)
        self.assertIs(label_b, label_c)
        # verify empty labesl raise an exception
        with self.assertRaises(EmptyLabel):
            the_factory.fromstr('')
        # check behavior with long labels
        with self.assertRaises(LabelTooLong):
            the_factory.fromstr('x' * 100)
        # check strings that cannot be converted to IDNA labels
        with self.assertRaises(UnicodeError):
            the_factory.fromstr(chr(0x80))

    def test_mixed(self) -> None:
        the_factory = label.LabelFactory()
        # add one label via each method
        non_factory_label = label.Label(b'aaa')
        label_a = the_factory.fromlabel(non_factory_label)
        label_b = the_factory.frombytes(b'bbb')
        label_c = the_factory.fromstr('ccc')
        # test label created using other methods
        label_a_bytes = the_factory.frombytes(b'aaa')
        self.assertIs(label_a, label_a_bytes)
        label_a_str = the_factory.fromstr('aaa')
        self.assertIs(label_a, label_a_str)
        # test bytes created using other methods
        label_b_label = the_factory.fromlabel(label_b)
        self.assertIs(label_b, label_b_label)
        label_b_str = the_factory.fromstr('bbb')
        self.assertIs(label_b, label_b_str)
        # test str created using other methods
        label_c_label = the_factory.fromlabel(label_c)
        self.assertIs(label_c, label_c_label)
        label_c_bytes = the_factory.frombytes(b'ccc')
        self.assertIs(label_c, label_c_bytes)


if __name__ == '__main__':
    unittest.main()
