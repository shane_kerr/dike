class NameCompressionLoop(Exception):
    pass


class BadNameEncoding(Exception):
    pass


def _get_name_from_packet(pkt, ofs, name_cache):
    labels = []
    start_ofs = ofs
    while True:
        label_len = pkt[ofs]
        if label_len == 0:
            name_cache[start_ofs] = labels
            return labels, ofs+1-start_ofs
        if label_len >= 192:
            compression_ofs = ((label_len - 192) << 8) | pkt[ofs+1]
            if compression_ofs >= start_ofs:
                msg = (f"Error decoding name at byte {ofs}: "
                       f"loop in name compression")
                raise NameCompressionLoop(msg)
            if ofs in name_cache:
                compressed_labels = name_cache[ofs]
            else:
                compressed_labels, _ = _get_name_from_packet(pkt,
                                                             compression_ofs,
                                                             name_cache)
            labels.extend(compressed_labels)
            return labels, ofs+2-start_ofs
        if label_len >= 64:
            msg = (f"Error decoding name at byte {ofs}: " +
                   f"invalid label length {label_len}")
            raise BadNameEncoding(msg)
        ofs += 1
        labels.append(pkt[ofs:ofs+label_len])
        ofs += label_len


def escape_char_str(char_str):
    """
    Escape a DNS character string, which is 0 to 255 bytes.

    We always use double-quotes around it. We escape the double-quote
    and backslash characters, as well as any non-printable characters.
    """
    chars = []
    for ch in char_str:
        if ch in (ord('"'), ord('\\')):
            chars.append("\\" + chr(ch))
        elif (ch < 32) or (ch >= 127):
            chars.append(f"\\{ch:03}")
        else:
            chars.append(chr(ch))
    return '"' + ''.join(chars) + '"'
