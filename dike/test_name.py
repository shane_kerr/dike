import ipaddress
import unittest

from dike.errors import EmptyLabel, LabelTooLong, LabelHasDot, NameTooLong
import dike.name as name


class TestName(unittest.TestCase):
    def test_init_empty(self) -> None:
        # verify that the default name works
        default_name = name.Name()
        default_name._invariant()
        self.assertEqual('.', str(default_name))
        self.assertEqual(b'.', bytes(default_name))
        # test with various forms of nothing
        with self.assertRaises(EmptyLabel):
            name.Name.fromstr('')
        with self.assertRaises(EmptyLabel):
            name.Name.frombytes(b'')
        # we also consider "." and b"." to be equivalent to empty init
        str_root_name = name.Name.fromstr(".")
        str_root_name._invariant()
        self.assertEqual(default_name, str_root_name)
        bytes_root_name = name.Name.frombytes(b".")
        bytes_root_name._invariant()
        self.assertEqual(default_name, bytes_root_name)

    def test_init_too_long(self) -> None:
        # Initialization must be okay with names up to 253 octets.

        # We use 4 labels of 62 octets, which gives us:
        #  62 + 1 + 62 + 1 + 62 + 1 + 62 = 251
        # We then add ".e", which is 2 more octets, giving us 253 octets.
        foo = name.Name.fromstr(("a" * 62) + "." + ("b" * 62) + "." +
                                ("c" * 62) + "." + ("d" * 62) + "." + "e")
        foo._invariant()

        # Next add one more octet and ensure that this is not allowed.
        with self.assertRaises(NameTooLong):
            name.Name.fromstr(str(foo) + "e")

        # And also try a VERY long name.
        with self.assertRaises(NameTooLong):
            name.Name.fromstr(((("x" * 63) + ".") * 20) + ("x" * 63))

        # Try a label too long.
        with self.assertRaises(LabelTooLong):
            name.Name.fromstr("y" * 64 + ".com")

        # Ensure we check label too long from straight invocation.
        with self.assertRaises(LabelTooLong):
            name.Name((b"z" * 64,))

    def test_init(self) -> None:
        # Verify that a simple string name works.
        example_str_name = name.Name.fromstr("example")
        example_str_name._invariant()
        self.assertEqual('example', str(example_str_name))
        self.assertEqual(b'example', bytes(example_str_name))
        # Verify that a simple bytes name works.
        test_byte_name = name.Name.frombytes(b'test')
        test_byte_name._invariant()
        self.assertEqual('test', str(test_byte_name))
        self.assertEqual(b'test', bytes(test_byte_name))
        # Okay, now try names with labels.
        multilabel_name = name.Name((b'www', b'example', b'com'))
        multilabel_name._invariant()
        self.assertEqual('www.example.com', str(multilabel_name))
        self.assertEqual(b'www.example.com', bytes(multilabel_name))

    def test_init_dot_label(self) -> None:
        dot_name = name.Name(((b'this.is.sparta!'),))
        dot_name._invariant()
        with self.assertRaises(LabelHasDot):
            str(dot_name)
        with self.assertRaises(LabelHasDot):
            bytes(dot_name)

    def test_init_empty_label(self) -> None:
        with self.assertRaises(EmptyLabel):
            name.Name.fromstr("foo..")
        with self.assertRaises(EmptyLabel):
            name.Name.frombytes(b"bar..baz")
        with self.assertRaises(EmptyLabel):
            name.Name(((b'www'),
                       (b''),
                       (b'com'),))

    def test_init_fqdn(self) -> None:
        fqdn_name1 = name.Name.fromstr("1.name.example.")
        fqdn_name1._invariant()
        pqdn_name1 = name.Name.frombytes(b"1.name.example")
        pqdn_name1._invariant()
        fqdn_name1 == pqdn_name1
        fqdn_name2 = name.Name.frombytes(b"2.name.example.")
        fqdn_name2._invariant()
        pqdn_name2 = name.Name.fromstr("2.name.example")
        pqdn_name2._invariant()
        fqdn_name2 == pqdn_name2

    def test_init_name(self) -> None:
        name_the_first = name.Name.fromstr("test.example.test")
        name_the_first._invariant()
        name_the_second = name.make_name(name_the_first)
        name_the_second._invariant()
        self.assertIs(name_the_first, name_the_second)
        self.assertEqual(name_the_first, name_the_second)

    def test_bad_label(self) -> None:
        with self.assertRaises(UnicodeError):
            name.Name.fromstr(chr(0x80))

    def test_comparison(self) -> None:
        name_aaa = name.Name.fromstr("aaa")
        name_aaa._invariant()
        name_AAA = name.Name.frombytes(b"AAA")
        name_AAA._invariant()
        self.assertEqual(name_aaa, name_AAA)
        self.assertLessEqual(name_aaa, name_AAA)
        self.assertGreaterEqual(name_aaa, name_AAA)
        # Comparisons create and store the _reversed_labels member, so
        # check our invariant again.
        name_aaa._invariant()
        name_AAA._invariant()
        name_bbb = name.make_name("bbb")
        name_bbb._invariant()
        self.assertNotEqual(name_aaa, name_bbb)
        self.assertLess(name_aaa, name_bbb)
        self.assertGreater(name_bbb, name_aaa)
        name_aaa_aaa = name.make_name(b"aaa.aaa")
        name_aaa_aaa._invariant()
        self.assertNotEqual(name_aaa, name_aaa_aaa)

        # We do this with assertFalse() since we want an __eq__()
        # comparison that fails.
        self.assertFalse(name_aaa == name_aaa_aaa)

        self.assertLess(name_aaa, name_aaa_aaa)
        self.assertGreater(name_aaa_aaa, name_aaa)
        self.assertLess(name_aaa_aaa, name_bbb)
        self.assertGreater(name_bbb, name_aaa_aaa)

        # Do some comparisons with strings/bytes/lists/tuples
        self.assertEqual(name_aaa, "AAA")
        self.assertEqual(name_bbb, b"BbB")

        # Try equality and non-equality with an unsupported type
        self.assertNotEqual(name_aaa, 123)
        self.assertTrue(name_aaa != 123)

        # Try comparison with an unsupported type
        self.assertFalse(name_aaa == 1)
        with self.assertRaises(TypeError):
            name_bbb < 3.14  # type: ignore

    def test_canonical(self) -> None:
        test_name = name.Name.fromstr("Zippity.do.DAH", canonicalize=True)
        test_name._invariant()
        self.assertEqual(test_name, "zippity.do.dah")
        # Name._canonical() modifies test_name, so check our invariant again.
        test_name._invariant()

        empty_name = name.Name(canonicalize=True)
        empty_name._invariant()
        self.assertEqual(empty_name._canonical(), ())
        empty_name._invariant()

        short_name = name.Name.fromstr("Zuul", canonicalize=True)
        short_name._invariant()
        self.assertEqual(short_name, b'zuul')
        short_name._invariant()

    def test_ishost(self) -> None:
        a_host = name.Name()
        self.assertFalse(a_host.ishost())
        a_host._invariant()
        # Name.ishost() modifies test_name, so check our invariant again.
        self.assertFalse(a_host.ishost())
        b_host = name.Name.fromstr("b")
        self.assertTrue(b_host.ishost())
        self.assertTrue(b_host.ishost())
        another_host = name.Name.fromstr('hosty')
        self.assertTrue(another_host.ishost())
        self.assertTrue(another_host.ishost())
        not_host = name.Name.fromstr('!host')
        self.assertFalse(not_host.ishost())
        self.assertFalse(not_host.ishost())
        not_host2 = name.Name.fromstr('host!')
        self.assertFalse(not_host2.ishost())
        self.assertFalse(not_host2.ishost())
        not_host3 = name.Name.fromstr('ho!st')
        self.assertFalse(not_host3.ishost())
        self.assertFalse(not_host3.ishost())

    def test_isroot(self) -> None:
        root = name.Name()
        self.assertTrue(root.isroot())
        nonroot = name.Name.fromstr("nonroot")
        self.assertFalse(nonroot.isroot())

    def test_to_presentation(self) -> None:
        normal_name = name.Name.fromstr("normal.name")
        self.assertEqual(normal_name.to_presentation(), "normal.name.")
        empty_name = name.Name()
        self.assertEqual(empty_name.to_presentation(), ".")
        dot_name = name.Name((b".", b"in", b"com"))
        self.assertEqual(dot_name.to_presentation(), "\\..in.com.")
        non_ascii_name = name.Name((bytes([169]),))
        self.assertEqual(non_ascii_name.to_presentation(), "\\169.")

    def test_to_wire(self) -> None:
        root = name.Name()
        self.assertEqual(root.to_wire(), b"\0")
        single = name.Name.fromstr("test")
        self.assertEqual(single.to_wire(), b"\x04test\0")
        multiple = name.Name.fromstr("this.is.a.test")
        self.assertEqual(multiple.to_wire(), b"\x04this\x02is\x01a\x04test\0")

    def test_hash(self) -> None:
        name_a = name.Name.fromstr("a.test")
        name_a._invariant()
        name_b = name.Name.fromstr("b.test")
        name_b._invariant()
        name_big_b = name.Name.fromstr("B.TeSt")
        name_big_b._invariant()
        self.assertNotEqual(hash(name_a), hash(name_b))
        self.assertNotEqual(hash(name_a), hash(name_big_b))
        self.assertEqual(hash(name_b), hash(name_big_b))
        name_a._invariant()
        name_b._invariant()
        name_big_b._invariant()

    def test_repr(self) -> None:
        ze_name = name.Name.fromstr("an.example")
        ze_name._invariant()
        self.assertEqual(repr(ze_name), "Name.fromstr('an.example')")
        no_name = name.Name()
        no_name._invariant()
        self.assertEqual(repr(no_name), "Name()")
        dot_name = name.Name(((b"slashdot.org"),))
        dot_name._invariant()
        self.assertEqual(repr(dot_name), "Name([b'slashdot.org'])")
        bad_unicode_name = name.Name((bytes([0x80]),))
        self.assertEqual(repr(bad_unicode_name), "Name.frombytes(b'\\x80')")

    def test_len(self) -> None:
        ze_name = name.Name()
        ze_name._invariant()
        self.assertEqual(len(ze_name), 0)
        ye_name = name.Name.fromstr("foo")
        ye_name._invariant()
        self.assertEqual(len(ye_name), 1)
        xe_name = name.Name.fromstr("foo.bar")
        xe_name._invariant()
        self.assertEqual(len(xe_name), 2)

    def test_contains(self) -> None:
        we_name = name.Name.fromstr("www.test.name.example")
        we_name._invariant()
        for a_label in we_name:
            self.assertTrue(a_label in we_name)
        self.assertTrue("test" in we_name)
        self.assertTrue("www.test" in we_name)
        self.assertTrue("test.name" in we_name)
        self.assertTrue("name.example" in we_name)
        self.assertTrue("www.test.name" in we_name)
        self.assertTrue("test.name.example" in we_name)
        self.assertTrue("www.test.name.example" in we_name)
        self.assertFalse("moar.www.test.name.example" in we_name)
        self.assertFalse("www2.test" in we_name)

    def test_iter(self) -> None:
        it_girl = name.Name()
        it_girl._invariant()
        with self.assertRaises(StopIteration):
            next(iter(it_girl))
        not_it_girl = name.Name.fromstr("a.one.a.two.a.one.two.three")
        list_of_labels = []
        for it_label in not_it_girl:
            list_of_labels.append(it_label)
        self.assertTupleEqual(tuple(list_of_labels),
                              tuple(reversed(not_it_girl._reversed_labels)))

    def test_add(self) -> None:
        a_name = name.Name.fromstr("man")
        a_name._invariant()
        b_name = name.Name.fromstr("thing")
        b_name._invariant()
        # test __add__
        self.assertEqual(a_name + "grove", name.Name.fromstr("man.grove"))
        a_name._invariant()
        self.assertEqual(a_name + b_name, name.Name.fromstr("man.thing"))
        a_name._invariant()
        b_name._invariant()
        self.assertEqual(a_name + b'go', name.Name.fromstr("man.go"))
        a_name._invariant()
        # test __radd__
        self.assertEqual("super" + a_name, name.Name.fromstr("super.man"))
        a_name._invariant()
        self.assertEqual(name.Name.fromstr("the") + b_name,
                         name.Name.fromstr("the.thing"))
        b_name._invariant()
        with self.assertRaises(TypeError):
            a_name + 42  # type: ignore

    def test_getitem(self) -> None:
        v_name = name.Name.fromstr("www.penguin.aq")
        v_name._invariant()
        self.assertEqual(b"www", v_name[0])
        self.assertEqual(b"penguin", v_name[1])
        self.assertEqual(b"aq", v_name[2])
        with self.assertRaises(IndexError):
            v_name[3]
        with self.assertRaises(IndexError):
            v_name[666]
        self.assertEqual(b"aq", v_name[-1])
        self.assertEqual(b"penguin", v_name[-2])
        self.assertEqual(b"www", v_name[-3])
        with self.assertRaises(IndexError):
            v_name[-4]
        with self.assertRaises(IndexError):
            v_name[-123]
        self.assertEqual(name.Name.fromstr("www.penguin"), v_name[:2])
        self.assertEqual(name.Name.fromstr("www.penguin"), v_name[0:2])
        self.assertEqual(name.Name.fromstr("www.penguin"), v_name[0:-1])
        self.assertEqual(name.Name.fromstr("penguin.aq"), v_name[1:])
        self.assertEqual(name.Name.fromstr("penguin.aq"), v_name[1:3])
        self.assertEqual(name.Name.fromstr("www.aq"), v_name[0::2])

    def test_Xwith(self) -> None:
        empty_name = name.Name()
        short_name = name.Name.fromstr("x")
        long_name = name.Name.fromstr("ipso.facto.quid.pro.quo")
        ptr = ipaddress.ip_address("bad::c0de").reverse_pointer
        ptr_name = name.Name.fromstr(ptr)

        # The empty name does end/start with empty names.
        self.assertTrue(empty_name.endswith(empty_name))
        self.assertTrue(empty_name.endswith(''))
        self.assertTrue(empty_name.endswith(b''))
        self.assertTrue(empty_name.startswith(empty_name))
        self.assertTrue(empty_name.startswith(''))
        self.assertTrue(empty_name.startswith(b''))
        # The empty name does not end/start with anything else.
        self.assertFalse(empty_name.endswith(tuple()))
        self.assertFalse(empty_name.endswith(short_name))
        self.assertFalse(empty_name.endswith('Y'))
        self.assertFalse(empty_name.endswith(('xyz',)))
        self.assertFalse(empty_name.endswith(long_name))
        self.assertFalse(empty_name.endswith('1.0.168.192.in-addr.arpa'))
        self.assertFalse(empty_name.endswith(ptr_name))
        self.assertFalse(empty_name.startswith(tuple()))
        self.assertFalse(empty_name.startswith(short_name))
        self.assertFalse(empty_name.startswith('Y'))
        self.assertFalse(empty_name.startswith(('xyz',)))
        self.assertFalse(empty_name.startswith(long_name))
        self.assertFalse(empty_name.startswith('1.0.168.192.in-addr.arpa'))
        self.assertFalse(empty_name.startswith(ptr_name))

        # Short names also end/start with empty names.
        self.assertTrue(short_name.endswith(empty_name))
        self.assertTrue(short_name.endswith(''))
        self.assertFalse(short_name.endswith(tuple()))
        self.assertTrue(short_name.startswith(empty_name))
        self.assertTrue(short_name.startswith(''))
        self.assertFalse(short_name.startswith(tuple()))
        # And short names end/start with themselves.
        self.assertTrue(short_name.endswith(short_name))
        self.assertTrue(short_name.endswith('x'))
        self.assertTrue(short_name.startswith(short_name))
        self.assertTrue(short_name.startswith('x'))
        # Short names do not end/start with other things.
        self.assertFalse(short_name.endswith('Y'))
        self.assertFalse(short_name.endswith(('xyz',)))
        self.assertFalse(short_name.endswith(long_name))
        self.assertFalse(short_name.endswith('1.0.168.192.in-addr.arpa'))
        self.assertFalse(short_name.endswith(ptr_name))
        self.assertFalse(short_name.startswith('Y'))
        self.assertFalse(short_name.startswith(('xyz',)))
        self.assertFalse(short_name.startswith(long_name))
        self.assertFalse(short_name.startswith('1.0.168.192.in-addr.arpa'))
        self.assertFalse(short_name.startswith(ptr_name))

        # Long names _also_ end/start with empty names.
        self.assertTrue(long_name.endswith(empty_name))
        self.assertTrue(long_name.endswith(''))
        self.assertFalse(long_name.endswith(tuple()))
        self.assertTrue(long_name.startswith(empty_name))
        self.assertTrue(long_name.startswith(''))
        self.assertFalse(long_name.startswith(tuple()))
        # And long names also end/start with themselves.
        self.assertTrue(long_name.endswith(long_name))
        self.assertTrue(long_name.endswith('ipso.facto.quid.pro.quo'))
        self.assertTrue(long_name.startswith(long_name))
        self.assertTrue(long_name.startswith('ipso.facto.quid.pro.quo'))
        # Long names might end with other names.
        self.assertTrue(long_name.endswith('facto.quid.pro.quo'))
        self.assertTrue(long_name.endswith('quid.pro.quo'))
        self.assertTrue(long_name.endswith('pro.quo'))
        self.assertTrue(long_name.endswith('quo'))
        self.assertTrue(long_name.endswith('.'))
        # Long names might start with other names.
        self.assertTrue(long_name.startswith('ipso.facto.quid.pro'))
        self.assertTrue(long_name.startswith('ipso.facto.quid'))
        self.assertTrue(long_name.startswith('ipso.facto'))
        self.assertTrue(long_name.startswith('ipso'))
        self.assertTrue(long_name.startswith('.'))
        # Long names don't end/start with everything, of course.
        self.assertFalse(long_name.endswith('xxx.quid.pro.quo'))
        self.assertFalse(long_name.endswith('quid.pr0.quo'))
        self.assertFalse(long_name.startswith('ipso.facto.quid.yyy'))
        self.assertFalse(long_name.startswith('ipso.f4cto.quid'))

        # A endswith()/startswith() call does _not_ match if we start
        # past the start. (This is a bit weird, but it matches the
        # behavior of the built-in str.endswith() function.
        self.assertFalse(empty_name.endswith('', 1))
        self.assertFalse(empty_name.startswith('', 1))
        # Check matches against empty if we start at the start.
        self.assertTrue(empty_name.endswith('', 0))
        self.assertTrue(empty_name.startswith('', 0))
        # Check various starting positions for short name.
        self.assertTrue(short_name.endswith('', 0))
        self.assertTrue(short_name.endswith('', 1))
        self.assertFalse(short_name.endswith('', 2))
        self.assertTrue(short_name.endswith('x', 0))
        self.assertFalse(short_name.endswith('q', 0))
        self.assertTrue(short_name.startswith('', 0))
        self.assertTrue(short_name.startswith('', 1))
        self.assertFalse(short_name.startswith('', 2))
        self.assertTrue(short_name.startswith('x', 0))
        self.assertFalse(short_name.startswith('q', 0))

        # Check empty labels in strings or bytes.
        with self.assertRaises(EmptyLabel):
            empty_name.endswith('foo..example')
        with self.assertRaises(EmptyLabel):
            empty_name.startswith('foo..example')
        with self.assertRaises(EmptyLabel):
            empty_name.endswith(b'bar..test')
        with self.assertRaises(EmptyLabel):
            empty_name.startswith(b'bar..test')

        # Check using multiple suffixes.
        self.assertFalse(short_name.endswith(('a', 'b', 'c',)))
        self.assertTrue(short_name.endswith(('x', 'b', 'c',)))
        self.assertTrue(short_name.endswith(('a', 'b', 'x',)))
        self.assertFalse(short_name.endswith(tuple()))
        self.assertFalse(short_name.startswith(('a', 'b', 'c',)))
        self.assertTrue(short_name.startswith(('x', 'b', 'c',)))
        self.assertTrue(short_name.startswith(('a', 'b', 'x',)))
        self.assertFalse(short_name.startswith(tuple()))

        # Check using a different starting point.
        self.assertFalse(empty_name.endswith('.', 99))
        self.assertTrue(short_name.endswith(empty_name, 1))
        self.assertTrue(long_name.endswith('pro.quo', 0))
        self.assertTrue(long_name.endswith('pro.quo', 1))
        self.assertTrue(long_name.endswith('pro.quo', 2))
        self.assertTrue(long_name.endswith('pro.quo', 3))
        self.assertFalse(long_name.endswith('pro.quo', 4))
        self.assertFalse(long_name.endswith('pro.quo', 5))
        self.assertFalse(empty_name.startswith('.', 99))
        self.assertTrue(short_name.startswith(empty_name, 1))
        self.assertTrue(long_name.startswith('ipso.facto', 0))
        self.assertFalse(long_name.startswith('ipso.facto', 1))
        self.assertTrue(long_name.startswith('facto', 1))
        self.assertFalse(long_name.startswith('facto', 2))

        # And finally check using an ending point.
        self.assertTrue(empty_name.endswith('.', end=99))
        self.assertTrue(short_name.endswith('.', start=1, end=0))
        self.assertTrue(long_name.endswith('facto.quid.pro', start=1, end=4))
        self.assertFalse(long_name.endswith('facto.quid.pro', start=2, end=5))
        self.assertTrue(long_name.endswith('quid.pro.quo', start=2, end=5))
        self.assertTrue(empty_name.startswith('.', end=99))
        self.assertTrue(short_name.startswith('.', start=1, end=0))
        self.assertTrue(long_name.startswith('facto.quid.pro', start=1, end=4))
        self.assertFalse(long_name.startswith('facto.quid.pro',
                         start=2, end=5))
        self.assertTrue(long_name.startswith('quid.pro.quo', start=2, end=5))

    def test_make_name(self) -> None:
        from_str = name.make_name('love.coffee')
        from_str._invariant()
        self.assertEqual(from_str, 'love.coffee')

        from_bytes = name.make_name(b'coffee.love')
        from_bytes._invariant()
        self.assertEqual(from_bytes, b'coffee.love')

        from_list = name.make_name(['www', b'corp', 'com'])
        from_list._invariant()
        self.assertEqual(from_list, 'www.corp.com')
        from_tuple = name.make_name(('mx', 'evil', 'corp'))
        from_tuple._invariant()
        self.assertEqual(from_tuple, 'mx.evil.corp')

        self.assertEqual(name.make_name("."), ".")

        with self.assertRaises(TypeError):
            name.make_name(set())  # type: ignore
        with self.assertRaises(TypeError):
            name.make_name(('www', 'dot', 2.7))  # type: ignore

        from_name = name.make_name(from_str)
        from_name._invariant()
        self.assertEqual(from_str, from_name)

        canonical_name = name.make_name(from_bytes, canonicalize=True)
        canonical_name._invariant()
        self.assertEqual(from_bytes, canonical_name)


if __name__ == '__main__':
    unittest.main()
