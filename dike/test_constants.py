import unittest

import dike.constants as constants


class TestRTYPEName(unittest.TestCase):
    def test_type_name_normal(self) -> None:
        self.assertEqual(constants.type_name(constants.Type.AAAA),
                         "AAAA")
        self.assertEqual(constants.type_name(constants.Type.ANY),
                         "ANY")
        self.assertEqual(constants.type_name(constants.Type.NSAP_PTR),
                         "NSAP-PTR")

    def test_type_name_unknown(self) -> None:
        self.assertEqual(constants.type_name(22222), "TYPE22222")
        self.assertEqual(constants.type_name(0), "TYPE0")
        self.assertEqual(constants.type_name(65534), "TYPE65534")
        self.assertEqual(constants.type_name(65535), "TYPE65535")

    def test_type_name_ranges(self) -> None:
        with self.assertRaises(ValueError):
            constants.type_name(-1)
        with self.assertRaises(ValueError):
            constants.type_name(65536)
        with self.assertRaises(ValueError):
            constants.type_name(9999999999999999999999)


class TestClassName(unittest.TestCase):
    def test_class_name_normal(self) -> None:
        self.assertEqual(constants.class_name(constants.Class.IN), "IN")
        self.assertEqual(constants.class_name(constants.Class.ANY), "ANY")

    def test_class_name_unknown(self) -> None:
        self.assertEqual(constants.class_name(3333), "CLASS3333")
        self.assertEqual(constants.class_name(0), "CLASS0")
        self.assertEqual(constants.class_name(65534), "CLASS65534")
        self.assertEqual(constants.class_name(65535), "CLASS65535")

    def test_class_name_ranges(self) -> None:
        with self.assertRaises(ValueError):
            constants.class_name(-1)
        with self.assertRaises(ValueError):
            constants.class_name(65536)
        with self.assertRaises(ValueError):
            constants.class_name(9999999999999999999999)
