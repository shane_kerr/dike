import collections.abc

from dike.constants import class_name, type_name
from dike.edns import EDNSfromRR
from dike.header import HeaderPacket
from dike.name import Name
from dike.question import QuestionPacket
from dike.section import SectionPacket, AdditionalSectionPacket


class Message:
    def __init__(self, header, question, *,
                 answer=None, authority=None, additional=None,
                 edns=None):
        self.header = header
        # Since most messages only have a single question, we
        # special-case our constructor so that if we are
        # initialized with a list or tuple we support multiple
        # questions, but normally we have a single questions.
        if isinstance(question, (str, bytes, Name)):
            self.question = [question]
        elif isinstance(question, collections.abc.Collection):
            self.question = tuple(question)
        else:
            self.question = (question,)
        self.answer = answer
        self.authority = authority
        self.additional = additional
        self.edns = edns

    def to_wire(self, *, fix_section_counts=True):
        if fix_section_counts:
            self.header.qdcount = len(self.question)
            if self.answer:
                self.header.ancount = len(self.answer)
            else:
                self.header.ancount = 0
            if self.authority:
                self.header.nscount = len(self.authority)
            else:
                self.header.nscount = 0
            if self.additional:
                self.header.arcount = len(self.additional)
            else:
                self.header.arcount = 0
            if self.edns:
                self.header.arcount += 1

        # We special case this since the join() takes a lot of time otherwise.
        if len(self.question) != 1:
            question = b''.join(q.to_wire() for q in self.question)
        else:
            question = self.question[0].to_wire()

        if self.answer is None:
            answer = b''
        else:
            answer = self.answer.to_wire()

        if self.authority is None:
            authority = b''
        else:
            authority = self.authority.to_wire()

        if self.additional is None:
            additional = b''
        else:
            additional = self.additional.to_wire()

        if self.edns is None:
            edns = b''
        else:
            edns = self.edns.to_wire()

        return (self.header.to_wire() +
                question +
                answer + authority + additional +
                edns)

    # XXX: this should be a utility function, really...
    def dig_format(self):
        lines = []
        for line in self.header.dig_format():
            lines.append(line)
        # TODO: OPT PSEUDOSECTION
        if self.question:
            lines.extend(("", ";; QUESTION SECTION:"))
            for question in self.question:
                lines.append(question.dig_format())
        if self.answer and self.answer.rrs:
            lines.extend(("", ";; ANSWER SECTION:"))
            for rr in self.answer.rrs:
                rrname = rr.rrname.to_presentation()
                ownerrname_ttl = (rrname.ljust(23) + " " +
                                  str(rr.rrttl))
                lines.append(ownerrname_ttl.ljust(31) + " " +
                             class_name(rr.rrclass).ljust(7) + " " +
                             type_name(rr.rrtype).ljust(7) + " " +
                             rr.rrdata.to_presentation())
        if self.authority and self.authority.rrs:
            lines.extend(("", ";; AUTHORITY SECTION:"))
            for rr in self.authority.rrs:
                rrname = rr.rrname.to_presentation()
                ownerrname_ttl = (rrname.ljust(23) + " " +
                                  str(rr.rrttl))
                lines.append(ownerrname_ttl.ljust(31) + " " +
                             class_name(rr.rrclass).ljust(7) + " " +
                             type_name(rr.rrtype).ljust(7) + " " +
                             rr.rrdata.to_presentation())
        if self.authority and self.additional.rrs:
            lines.extend(("", ";; ADDITIONAL SECTION:"))
            for rr in self.additional.rrs:
                rrname = rr.rrname.to_presentation()
                ownerrname_ttl = (rrname.ljust(23) + " " +
                                  str(rr.rrttl))
                lines.append(ownerrname_ttl.ljust(31) + " " +
                             class_name(rr.rrclass).ljust(7) + " " +
                             type_name(rr.rrtype).ljust(7) + " " +
                             rr.rrdata.to_presentation())
        return lines


class MessagePacket(Message):
    def __init__(self, pkt):
        self.header = HeaderPacket(pkt)
        ofs = self.header.size()
        msg_question = []
        nc = {}
        for _ in range(self.header.qdcount):
            question = QuestionPacket(pkt, ofs, nc)
            msg_question.append(question)
            ofs += question.size()
        self.question = tuple(msg_question)
        self.answer = SectionPacket(pkt, ofs, self.header.ancount, nc)
        ofs += self.answer.size()
        self.authority = SectionPacket(pkt, ofs, self.header.nscount, nc)
        ofs += self.authority.size()
        self.additional = AdditionalSectionPacket(pkt, ofs,
                                                  self.header.arcount, nc)
        ofs += self.additional.size()
        self._size = ofs

        if self.additional.opt_rr:
            opt_rr = self.additional.opt_rr[-1]
            extended_rcode = opt_rr.rrttl >> 24
            version = (opt_rr.rrttl >> 16) & 0xFF
            do = (opt_rr.rrttl >> 15) & 1
            self.edns = EDNSfromRR(payload_size=opt_rr.rrclass,
                                   extended_rcode=extended_rcode,
                                   version=version,
                                   do=do,
                                   rrdata=opt_rr.rrdata)
        else:
            self.edns = None
