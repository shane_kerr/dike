"""
The errors module collects our error types.

Our error tree looks like::

  DikeException
  |
  +-- NameException
      |
      +- EmptyLabel
      |
      +- LabelTooLong
      |
      +- NameTooLong
      |
      +- LabelHasDot
"""


class DikeException(Exception):
    """
    Parent of all exceptions raised by the dike library.
    """


class NameException(DikeException):
    """
    Exceptions raised when using Names.
    """


class EmptyLabel(NameException, ValueError):
    """
    Exception raised when trying to initialize a label with an empty
    value.
    """


class LabelTooLong(NameException, ValueError):
    """
    Exception raised when trying to initialize a label of more than 63
    octets.
    """


class NameTooLong(NameException, ValueError):
    """
    Exception raised when trying to initialize a name of more than
    253 octets.

    This corresponds to a name longer than 255 octets when converted
    to wire format, since the wire format has a length indicator for
    the first label and a terminating length indicator of 0.
    """


class LabelHasDot(NameException, ValueError):
    """
    Exception raised when trying to convert a name to a string or
    bytes value which has a dot, ``'.'``,  *in* a label. In that case,
    we cannot convert to a string or bytes value, since it would be
    impossible to tell which ``'.'`` are separators and which are part
    of a label.

    For example::

        # very legal and very cool
        label_with_dot = Label('label.with.dot')
        le_name = Name((label_with_dot, 'example', 'com'))

        # but this will raise LabelHasDot
        print(le_name)

    If you need to work with names that might possibly contain a dot
    in them (for example if processing packets from the Internet), you
    can treat labels separately, or use the wire or presentation
    format for names.
    """
