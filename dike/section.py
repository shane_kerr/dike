# XXX: organize by rrset?
# XXX: keep label cache optional?
import dike.constants
import dike.rr


class Section:
    def __init__(self, rrs=None):
        self.rrs = rrs

    def __len__(self):
        return len(self.rrs)

    # TODO: name compression
    # TODO: ordering (plus randomization)?
    def to_wire(self):
        return b''.join([rr.towire() for rr in self.rrs])

    def __getattr__(self, name):
        if name in ("rr_by_rrtype", "rr_by_rrname"):
            self.rr_by_rrtype = {}
            self.rr_by_rrname = {}
            for rr in self.rrs:
                self.rr_by_rrname.setdefault(rr.rrname, []).append(rr)
                self.rr_by_rrtype.setdefault(rr.rrtype, []).append(rr)
            return getattr(self, name)
        else:
            msg = f"'{type(self).__name__}' object has no attribute '{name}'"
            raise AttributeError(msg)


class SectionPacket(Section):
    def __init__(self, pkt, ofs, count, name_cache=None):
        self._pkt = pkt
        self._ofs = ofs
        self._count = count
        if name_cache is None:
            self._name_cache = {}
        else:
            self._name_cache = name_cache
        self._parsed = False

    def _parse(self):
        if self._parsed:
            return
        self.rrs = []
        self.rr_by_rrtype = {}
        self.rr_by_rrname = {}
        ofs = self._ofs
        for _ in range(self._count):
            rr = dike.rr.RRPacket(self._pkt, ofs, self._name_cache)
            # XXX: handle OPT here maybe?
            self.rrs.append(rr)
            self.rr_by_rrname.setdefault(rr.rrname, []).append(rr)
            self.rr_by_rrtype.setdefault(rr.rrtype, []).append(rr)
            ofs += rr.size()
        self._size = ofs - self._ofs
        self._parsed = True

    def parse(self):
        self._parse()
        for rr in self.rrs:
            rr.parse()

    def __getattr__(self, name):
        if name not in ("rrs", "rr_by_rtype", "rr_by_rrname"):
            msg = f"'{type(self).__name__}' object has no attribute '{name}'"
            raise AttributeError(msg)
        self._parse()
        return getattr(self, name)

    def size(self):
        self._parse()
        return self._size


class AdditionalSectionPacket(Section):
    def __init__(self, pkt, ofs, count, name_cache=None):
        self._pkt = pkt
        self._ofs = ofs
        self._count = count
        if name_cache is None:
            self._name_cache = {}
        else:
            self._name_cache = name_cache
        self._parsed = False

    def _parse(self):
        if self._parsed:
            return
        self.rrs = []
        self.rr_by_rrtype = {}
        self.rr_by_rrname = {}
        self.opt_rr = []
        ofs = self._ofs
        for _ in range(self._count):
            rr = dike.rr.RRPacket(self._pkt, ofs, self._name_cache)
            if rr.rrtype == dike.constants.Type.OPT:
                self.opt_rr.append(rr)
            else:
                self.rrs.append(rr)
                self.rr_by_rrname.setdefault(rr.rrname, []).append(rr)
                self.rr_by_rrtype.setdefault(rr.rrtype, []).append(rr)
            ofs += rr.size()
        self._size = ofs - self._ofs
        self._parsed = True

    def __getattr__(self, name):
        if name not in ("rrs", "rr_by_rtype", "rr_by_rrname", "opt_rr"):
            msg = f"'{type(self).__name__}' object has no attribute '{name}'"
            raise AttributeError(msg)
        self._parse()
        return getattr(self, name)

    def parse(self):
        self._parse()
        for rr in self.rrs:
            rr.parse()

    def size(self):
        self._parse()
        return self._size
