import numbers
import struct

import dike.constants
from dike.name import Name
import dike.rr
from dike.util import _get_name_from_packet


class rdata_mx(dike.rr.RData):
    """
    preference - integer between 0 and 65535
    exchange - dike.Name() instance
    """
    def __init__(self, *, preference, exchange):
        self.preference = preference
        self.exchange = exchange

    def _invariant(self):
        assert isinstance(self.preference, numbers.Integral)
        assert 0 <= self.preference <= 65535
        self.exchange._invariant()

    # length without name compression
    def __len__(self):
        return 2+len(self.exchange.to_wire())

    def to_presentation(self):
        return str(self.preference) + " " + self.exchange.to_presentation()


class MXTooShort(dike.rr.RRTooShort):
    pass


class rdata_packet_mx(rdata_mx):
    def __init__(self, pkt, ofs, rr_len, name_cache=None):
        self.pkt = pkt
        self.ofs = ofs
        self.mx_len = rr_len
        if name_cache is None:
            self.name_cache = {}
        else:
            self.name_cache = name_cache

    def __dir__(self):
        return ["preference", "exchange",
                "__init__", "__len__", "__dir__", "to_presentation()",
                "_invariant"]

    def __getattr__(self, name):
        if name == "preference":
            if self.mx_len < 2:
                raise MXTooShort()
            (self.preference,) = struct.unpack("!H",
                                               self.pkt[self.ofs:self.ofs+2])
            return self.preference

        if name == "exchange":
            try:
                (exchange_labels,
                 exchange_len) = _get_name_from_packet(self.pkt, self.ofs+2,
                                                       self.name_cache)
            except IndexError:
                raise MXTooShort()
            self.exchange = Name(exchange_labels)
            return self.exchange

        msg = f"'{type(self).__name__}' object has no attribute '{name}'"
        raise AttributeError(msg)

    def __len__(self):
        return self.ns_len


dike.rr.register_rr_packet_type(dike.constants.Type.MX, rdata_packet_mx)
