import numbers
import struct

from dike.name import Name
from dike.util import _get_name_from_packet


class RData:
    def __init__(self, rdata):
        self._rdata = rdata

    def __len__(self):
        return len(self._rdata)

    def to_presentation(self):
        return '\\# ' + str(len(self._rdata)) + ' ' + self._rdata.hex()

    # def to_wire(self):
    #    return self.rdata

# TODO: add Unknown RR type


class RR:
    """
    rrname - dike.Name() instance
    rrtype - integer between 0 and 65535
    rrclass - integer between 0 and 65535
    rrttl - integer between 0 and 4294967295
    rrdata - RData() instance
    """
    def __init__(self, *, rrname, rrtype, rrclass, rrttl, rrdata):
        self.rrname = rrname
        self.rrtype = rrtype
        self.rrclass = rrclass
        self.rrttl = rrttl
        self.rrdata = rrdata

    def _invariant(self):
        self.rrname._invariant()
        assert isinstance(self.rrtype, numbers.Integral)
        assert 0 <= self.rrtype < 2**16
        assert isinstance(self.rrclass, numbers.Integral)
        assert 0 <= self.rrclass < 2**16
        assert isinstance(self.rrttl, numbers.Integral)
        assert 0 <= self.rrttl < 2**32
        assert isinstance(self.rrdata, RData)

    # TODO: name compression
    def to_wire(self):
        rrname_labels = (bytes((len(bytes(label)),)) + bytes(label)
                         for label in self.rrname)
        return (b''.join(rrname_labels) +
                struct.pack("!HHIH", self.rrtype, self.rrclass,
                            self.rrttl, len(self.rrdata)) +
                self.rrdata.to_wire())


class RRTooShort(Exception):
    pass


_rr_packet_types = {}


def register_rr_packet_type(rrtype, rr_class):
    _rr_packet_types[rrtype] = rr_class


class RRPacket(RR):
    def __init__(self, pkt, ofs, name_cache=None):
        self.pkt = pkt
        self.ofs = ofs
        if name_cache is None:
            self.name_cache = {}
        else:
            self.name_cache = name_cache
        self._parsed = False

    def _parse(self):
        if self._parsed:
            return
        try:
            (self._name_labels,
             rrname_len) = _get_name_from_packet(self.pkt,
                                                 self.ofs,
                                                 self.name_cache)
            fixed_ofs = self.ofs + rrname_len
            (self.rrtype,
             self.rrclass,
             self.rrttl,
             rrdata_len) = struct.unpack("!HHIH",
                                         self.pkt[fixed_ofs:fixed_ofs+10])
            rrdata_ofs = fixed_ofs+10
            if self.rrtype in _rr_packet_types:
                packet_type = _rr_packet_types[self.rrtype]
                self.rrdata = packet_type(self.pkt, rrdata_ofs, rrdata_len,
                                          self.name_cache)
            else:
                rrdata = self.pkt[rrdata_ofs:rrdata_ofs+rrdata_len]
                self.rrdata = RData(rrdata)
            self._rr_size = rrdata_ofs + rrdata_len - self.ofs
        except IndexError:
            raise RRTooShort()

    def parse(self):
        self._parse()
        if getattr(self, 'rrname', None) is None:
            self.rrname = Name(self._name_labels)
        self.rrdata.parse()

    def __getattr__(self, name):
        self._parse()
        if name == "rrtype":
            return self.rrtype
        if name == "rrclass":
            return self.rrclass
        if name == "rrttl":
            return self.rrttl
        if name == "rrname":
            self.rrname = Name(self._name_labels)
            return self.rrname
        msg = f"'{type(self).__name__}' object has no attribute '{name}'"
        raise AttributeError(msg)

    def size(self):
        self._parse()
        return self._rr_size
