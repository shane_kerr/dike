import base64
import numbers
import struct
import time

import dike.constants
from dike.name import Name
import dike.rr
from dike.util import _get_name_from_packet


class rdata_rrsig(dike.rr.RData):
    """
    type_covered - integer between 0 and 65535
    algorithm - integer between 0 and 255
    labels - integer between 0 and 255
    original_ttl - integer between 0 and 4294967295
    sig_expire - integer between 0 and 4294967295
    sig_inception - integer between 0 and 4294967295
    key_tag - integer between 0 and 65535
    signers_name - dike.Name() instance
    signature - :py:class:`bytes`
    """
    def __init__(self, *,
                 type_covered, algorithm, labels, original_ttl,
                 sig_expire, sig_inception, key_tag, signers_name,
                 signature):
        self.type_covered = type_covered
        self.algorithm = algorithm
        self.labels = labels
        self.original_ttl = original_ttl
        self.sig_expire = sig_expire
        self.sig_inception = sig_inception
        self.key_tag = key_tag
        self.signers_name = signers_name
        self.signature = signature

    def _invariant(self):
        assert isinstance(self.type_covered, numbers.Integral)
        assert 0 <= self.type_covered < 2**16
        assert isinstance(self.algorithm, numbers.Integral)
        assert 0 <= self.algorithm < 2**8
        assert isinstance(self.labels, numbers.Integral)
        assert 0 <= self.labels < 2**8
        assert isinstance(self.original_ttl, numbers.Integral)
        assert 0 <= self.original_ttl < 2**32
        assert isinstance(self.sig_expire, numbers.Integral)
        assert 0 <= self.sig_expire < 2**32
        assert isinstance(self.sig_inception, numbers.Integral)
        assert 0 <= self.sig_inception < 2**32
        assert isinstance(self.key_tag, numbers.Integral)
        assert 0 <= self.key_tag < 2**16
        self.signers_name._invariant()
        assert self.signature != b''

    def __len__(self):
        return 18 + len(self.signers_name.to_wire()) + len(self.signature)

    def to_presentation(self):
        return (dike.constants.type_name(self.type_covered) + " " +
                str(self.algorithm) + " " +
                str(self.labels) + " " +
                str(self.original_ttl) + " " +
                time.strftime("%Y%m%d%H%M%S ",
                              time.gmtime(self.sig_expire)) +
                time.strftime("%Y%m%d%H%M%S ",
                              time.gmtime(self.sig_inception)) +
                str(self.key_tag) + " " +
                self.signers_name.to_presentation() + " " +
                base64.b64encode(self.signature).decode())


class RRSIGTooShort(dike.rr.RRTooShort):
    pass


class rdata_packet_rrsig(rdata_rrsig):
    def __init__(self, pkt, ofs, rr_len, name_cache=None):
        self.pkt = pkt
        self.ofs = ofs
        self.rrsig_len = rr_len
        if name_cache is None:
            self.name_cache = {}
        else:
            self.name_cache = name_cache

    def __dir__(self):
        return ["type_covered", "algorithm", "labels", "original_ttl",
                "sig_expire", "sig_inception", "key_tag", "signers_name",
                "signature",
                "__init__", "__len__", "__dir__", "to_presentation",
                "_invariant"]

    def __getattr__(self, name):
        if name in ("type_covered", "algorithm", "labels", "original_ttl",
                    "sig_expire", "sig_inception", "key_tag"):
            try:
                (self.type_covered,
                 self.algorithm,
                 self.labels,
                 self.original_ttl,
                 self.sig_expire,
                 self.sig_inception,
                 self.key_tag) = struct.unpack("!HBBIIIH",
                                               self.pkt[self.ofs:self.ofs+18])
            except IndexError:
                raise RRSIGTooShort()
            return getattr(self, name)

        if name not in ("signers_name", "signature"):
            msg = f"'{type(self).__name__}' object has no attribute '{name}'"
            raise AttributeError(msg)

        try:
            (signers_name_labels,
             signers_name_len) = _get_name_from_packet(self.pkt, self.ofs+18,
                                                       self.name_cache)
        except IndexError:
            raise RRSIGTooShort()

        self.signers_name = Name(signers_name_labels)
        sig_start = self.ofs + 18 + signers_name_len
        self.signature = self.pkt[sig_start:self.ofs+self.rrsig_len]
        return getattr(self, name)

    def __len__(self):
        return self.rrsig_len


dike.rr.register_rr_packet_type(dike.constants.Type.RRSIG, rdata_packet_rrsig)
