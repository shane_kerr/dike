import ipaddress

import dike.constants
import dike.rr

# TODO: memoize conversions?


class rdata_aaaa(dike.rr.RData):
    """
    address - ipaddress.IPv46ddress instance
    """
    def __init__(self, *, address):
        self.address = address

    def _invariant(self):
        assert isinstance(self.address, ipaddress.IPv6Address)

    def __len__(self):
        return 16

    def to_wire(self):
        return self.address.packed

    def to_presentation(self):
        return str(self.address)


class rdata_packet_aaaa(rdata_aaaa):
    def __init__(self, pkt, ofs, rr_len, name_cache=None):
        if rr_len != 16:
            raise dike.rr.RRTooShort()
        self._packed = pkt[ofs:ofs+16]

    def __dir__(self):
        return ["address",
                "__init__", "__len__", "__dir__", "to_presentation", "to_wire",
                "_invariant"]

    def __getattr__(self, name):
        if name == "address":
            self.address = ipaddress.IPv6Address(self._packed)
            return self.address

        msg = f"'{type(self).__name__}' object has no attribute '{name}'"
        raise AttributeError(msg)


dike.rr.register_rr_packet_type(dike.constants.Type.AAAA, rdata_packet_aaaa)
