import cProfile
import pstats

import socket
import multiprocessing

import dike


profile = False


def serve_dns(sock):
    while True:
        pkt, client_addr_info = sock.recvfrom(65536)
        query = dike.MessagePacket(pkt)

        hdr = query.header
        # XXX: just for test
        hdr.rcode = dike.RCODE.NOTIMP
        hdr.qr = dike.QR.RESPONSE
        hdr.rd = 0
        hdr.arcount = 0
        if query.edns:
            edns = dike.EDNS(do=0)
        else:
            edns = None
        response = dike.Message(hdr, query.question, edns=edns)
        sock.sendto(response.to_wire(), client_addr_info)


def main(use_multiple_processes=False):
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    addr = ('', 53535)
    sock.bind(addr)
    print(f"listening for DNS queries on {addr}")
    if use_multiple_processes:
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)
        threads = []
        threads.append(multiprocessing.Process(target=serve_dns, args=(sock,)))
        threads.append(multiprocessing.Process(target=serve_dns, args=(sock,)))
        threads.append(multiprocessing.Process(target=serve_dns, args=(sock,)))
        #threads.append(multiprocessing.Process(target=serve_dns, args=(sock,)))
        for thread in threads:
            thread.start()
    else:
        serve_dns(sock)


if __name__ == '__main__':
    if profile:
        pr = cProfile.Profile()
        pr.enable()
        try:
            main()
        except:
            pass
        pr.disable()
        print()
        ps = pstats.Stats(pr)
        ps.print_stats()
        ps.dump_stats('dnsecho.profile')
    else:
        # main(use_multiple_processes=True)
        main(use_multiple_processes=False)
