.. The Dike DNS Library documentation master file, created by
   sphinx-quickstart on Sat Oct 13 15:27:28 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

The Dike DNS Library
====================

Introduction 
============
The Dike DNS Library is intended to be a powerful and Pythonic library
for working with the DNS. It is a pure Python tool that aims to be
both correct and efficient, both for programmer time and for runtime
performance.

Contents
========
.. toctree::
   :maxdepth: 2

   name.rst
   constants.rst
   errors.rst
   PacketProcessing.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

