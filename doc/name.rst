Name Module
===========

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. automodule:: name

:py:class:`Name` Objects
-------------------------
.. autoclass:: name.Name
   :members:

Utility Functions
-----------------
.. autofunction:: make_name
